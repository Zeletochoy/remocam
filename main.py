import network
import time
import uio as io
import os
import picoweb
from machine import Pin
import urequests as requests
from camera import Camera


# Settings
AP_SSID = "RemoCam"
AP_PASS = "loljambe"
CAM_SSID = "DIRECT-pFC2:DSC-RX10M3"
CAM_PASS = "wVeuVxDG"
CAM_IP = "192.168.122.1"
CAM_PORT = 8080


# AP creation
ap = network.WLAN(network.AP_IF);
ap.active(True);
ap.config(essid=AP_SSID, authmode=network.AUTH_WPA_WPA2_PSK, password=AP_PASS);


# Connection to camera
nic = network.WLAN(network.STA_IF)
nic.active(True)
nic.connect(CAM_SSID, CAM_PASS)

print("Connecting to camera...")
while not nic.isconnected():
    time.sleep_ms(100)
print("Connected.")


# Initialize web server, GPIO and camera
app = picoweb.WebApp(__name__)
led = Pin(2, Pin.OUT)
cam = Camera(CAM_IP, CAM_PORT)


# Utils
def html_response(resp, text):
    yield from picoweb.start_response(resp, content_type="text/html")
    yield from resp.awrite(text)


# Webserver routes
@app.route("/")
def index(req, resp):
    yield from app.sendfile(resp, "index.html")

@app.route("/on")
def on(req, resp):
    led.off() # inverted
    yield from html_response(resp, "OK")

@app.route("/off")
def off(req, resp):
    led.on() # inverted
    yield from html_response(resp, "OK")

@app.route("/shoot")
def shoot(req, resp):
    res = cam.actTakePicture()
    print(res)
    yield from html_response(resp, "OK")

@app.route("/exec")
def pyexec(req, resp):
    code = yield from req.reader.read()
    pipe = io.StringIO()
    os.dupterm(pipe)
    try:
        exec(code)
    except Exception as e:
        pipe.write(str(e))
    finally:
        os.dupterm(None)
    yield from html_response(resp, pipe.getvalue())


app.run(host="0.0.0.0", port=80)
