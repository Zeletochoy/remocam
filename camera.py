import ujson as json
import urequests as requests


class Camera(object):
    def __init__(self, ip, port):
        self.base_url = "http://{}:{}".format(ip, port)
        # start recording mode
        self.api_call("startRecMode")
        # get available methods
        self.update_methods()


    def api_call(self, method, uri="/sony/camera", parse=True, **kwargs):
        url = self.base_url + uri
        # Format the call as json and set default values when not provided
        params = kwargs
        params["method"] = method
        defaults = {"params": [], "id": 1, "version": "1.0"}
        for param, val in defaults.items():
            if param not in params or params[param] is None:
                params[param] = val
        json_params = json.dumps(params)
        try:
            # Send the request and parse result if asked
            res = requests.post(url, data=json_params)
            return json.loads(res.text) if parse else res.text
        except Exception as e:
            print("ERROR: {}".format(repr(e)))
            return None


    def update_methods(self):
        res = self.api_call("getAvailableApiList")
        try:
            self.methods = set(res["result"][0])
        except Exception as e:
            print("ERROR: Invalid JSON in update_methods: {}".format(res))


    def __getattr__(self, name):
        # Normally defined attribute/method
        if name in self.__dict__:
            return self.__dict__[name]
        # Update available methods if not found
        if name not in self.methods:
            self.update_methods()
        # Wrapper function to call the API
        if name in self.methods:
            def _api(**kwargs):
                return self.api_call(name, **kwargs)
            return _api
        raise AttributeError("Camera object has no attribute " + name)
